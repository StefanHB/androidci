package com.example.cours

import android.content.Intent
import android.os.Bundle

import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cours.databinding.TwitterMainBinding
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class TwitterActivity : AppCompatActivity() {
    lateinit var binding : TwitterMainBinding;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.twitter_main)
        var submitButton: Button = findViewById(R.id.submit);
        val user = User("toto");
        binding.user = user;


        submitButton.setOnClickListener(
            {
                view -> login()
            }
        )
    }

    fun login() {
        var inputUsername: TextInputLayout = binding.usernameParent;
        // Ajouter text input edit text
        var inputEditText: TextInputEditText = binding.inputUsername;


        var usernameText = inputUsername.editText?.text.toString()
        var regex = "^[a-zA-Z0-9]{5,15}$".toRegex();
        var coordinatorLayout: ConstraintLayout = binding.layoutconnexion;
        if (usernameText != null) {
            if(usernameText.matches(regex)) {
                val snackbar = Snackbar
                    .make(coordinatorLayout, "Connexion réussie", Snackbar.LENGTH_LONG)
                snackbar.show()
                intent = Intent(this, TwitterFeed::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar
                    .make(coordinatorLayout, "Les identifiants renseignés ne sont pas correct", Snackbar.LENGTH_LONG)
                snackbar.show()
                /*
                inputEditText.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(p0: Editable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        TODO("Not yet implemented")
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        inputEditText.error = "Username n'est pas correctement renseigné"
                    }
                })*/
            }
        }
    }
}