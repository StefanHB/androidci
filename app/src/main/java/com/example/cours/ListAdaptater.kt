package com.example.cours

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListAdaptater(val data: ArrayList<Tweets>) : RecyclerView.Adapter<ListAdaptater.MyViewHolder>() {
    class MyViewHolder(row : View) : RecyclerView.ViewHolder(row) {
        val textView = row.findViewById<TextView>(R.id.tweet)
        val imageView = row.findViewById<ImageView>(R.id.imageTweet)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.twitter_listing_element, parent, false);
        return MyViewHolder(layout);
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textView.text = data[position].text
        holder.imageView.setImageResource(data[position].image)
    }

    override fun getItemCount(): Int = data.size

}