package com.example.cours

data class Tweets(
    val image: Int,
    val text: String,
    val id: Int,
)
