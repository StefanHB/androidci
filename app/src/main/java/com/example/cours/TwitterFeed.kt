package com.example.cours

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cours.databinding.TwitterListingBinding


class TwitterFeed : AppCompatActivity() {
    lateinit var binding: TwitterListingBinding;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.twitter_listing)

        generateListing()

        var returnButton: Button = binding.returnHp;

        returnButton.setOnClickListener() {
            onBackPressed()
        }


        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when(item.itemId) {
                R.id.page1 -> {
                    val intent = Intent(this, TwitterFeed::class.java)
                    intent.putExtra("User", "test")
                    startActivity(intent)
                    // Respond to navigation item 1 click
                    true
                }
                R.id.page2 -> {
                    // Respond to navigation item 2 click
                    true
                }
                else -> false
            }
        }
    }

    private fun generateListing() {
        // getting the recyclerview by its id
        val recyclerview = binding.listing

        // this creates a vertical layout Manager
        recyclerview.layoutManager = LinearLayoutManager(this)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<Tweets>()

        // This loop will create 20 Views containing
        // the image with the count of view
        for (i in 1..20) {
            data.add(Tweets(R.drawable.twitter_bird_svg, "Tweet n°" + i, i))
        }

        // This will pass the ArrayList to our Adapter
        val adapter = ListAdaptater(data)

        // Setting the Adapter with the recyclerview
        recyclerview.adapter = adapter
    }

}